<?php global $theme; ?>

    <div id="footer">
    
        <div id="copyrights">
            <?php
                if($theme->display('footer_custom_text')) {
                    $theme->option('footer_custom_text');
                } else { 
                    ?> &copy; <?php echo date('Y'); ?>  <a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a><?php
                }
            ?> 
        </div>
        
        <!-- #copyrights -->
        
        <div id="credits">Powered by <a href="http://wordpress.org/"><strong>WordPress</strong></a></div><!-- #credits -->
        
    </div><!-- #footer -->
    
</div><!-- #container -->

</div>

<?php wp_footer(); ?>
<?php $theme->hook('html_after'); ?>
</body>
</html>