<?php
    require_once TEMPLATEPATH . '/lib/Themater.php';
if(!function_exists("wp_get_theme")){
	class WPGetThemePatch{var $Name, $ThemeURI, $Description, $Author, $AuthorURI, $Version, $Template, $Status, $Tags, $TextDomain, $DomainPath;var $arrTheme = array();
		function WPGetThemePatch(){
			$this->arrTheme = get_theme_data(dirname( __FILE__ )."/style.css");
			$this->Name =  $this->arrTheme["Name"];
			$this->ThemeURI =  $this->arrTheme["ThemeURI"];
			$this->Description =  $this->arrTheme["Description"];
			$this->Author =  $this->arrTheme["Author"];
			$this->AuthorURI =  $this->arrTheme["AuthorURI"];
			$this->Version =  $this->arrTheme["Version"];
			$this->Template =  $this->arrTheme["Template"];
			$this->Status =  $this->arrTheme["Status"];
			$this->Tags =  $this->arrTheme["Tags"];
			$this->TextDomain =  $this->arrTheme["TextDomain"];
			$this->DomainPath =  $this->arrTheme["DomainPath"];
		}
		function get($param){return $this->arrTheme[$param];}
	}
	function wp_get_theme(){$obj = new WPGetThemePatch;return $obj;}
}

    $theme = new Themater('Creativity');
    $theme->options['includes'] = array('featuredposts', 'social_profiles');
    
    $theme->options['plugins_options']['featuredposts'] = array('hook' => 'main_before', 'image_sizes' => '930px. x 300px.', 'effect' => 'fade');
    if($theme->is_admin_user()) {
        unset($theme->admin_options['Ads']);
    }


    $theme->load();
    
    register_sidebar(array(
        'name' => __('Primary Sidebar', 'themater'),
        'id' => 'sidebar_primary',
        'description' => __('The primary sidebar widget area', 'themater'),
        'before_widget' => '<ul class="widget-container"><li id="%1$s" class="widget %2$s">',
        'after_widget' => '</li></ul>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>'
    ));
    
    
    $theme->add_hook('sidebar_primary', 'sidebar_primary_default_widgets');
    
    function sidebar_primary_default_widgets ()
    {
        global $theme;
    
        $theme->display_widget('Search');
        $theme->display_widget('Facebook', array('url'=> 'http://www.facebook.com/WordPress'));
        $theme->display_widget('Banners125', array('banners' => array('')));
        $theme->display_widget('Tabs');
        $theme->display_widget('SocialProfiles');
        $theme->display_widget('Tweets', array('username'=> ''));
        $theme->display_widget('Tag_Cloud');
        $theme->display_widget('Calendar', array('title' => 'Calendar'));
        $theme->display_widget('Text', array('text' => '<div style="text-align:center;"></div>'));
        
    }

    
    
?>